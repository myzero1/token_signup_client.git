# token_signup_client

#### 介绍
token_signup(token签到)，原理是通过中间层，减少token的有效时间，把有效时间缩减到10-20秒。
token_signup_client主要作用是在发送请求时自动生成添加Z1token请求头，供中间层验证有效性。

#### 软件架构
软件架构说明


#### 安装教程

1.  安装
```code

<script src="https://cdn.jsdelivr.net/npm/token_signup_client@0.1.0/dist/z1token_signup-min.js"></script>

```

2.  npm安装
```code

npm install token_signup_client

```


#### 使用说明
0. ``******注意****`` 
```
1   必须在发送ajax请求之前就已经执行了z1token_signup()，否则请求头中不会自动添加Z1token,从而导致后期的验证返回403

2   token的名称必须为token

3   url中含有token或者包含Authorization

4   token_signup_proxy 可以设置哪一些uri不需要验证

```

1.  使用
```code
<script src="https://cdn.jsdelivr.net/npm/token_signup_client@0.1.0/dist/z1token_signup-min.js"></script>
<script">z1token_signup()</script>

```

2.  npm使用
```code

import token_signup_client from 'token_signup_client'
token_signup_client()

```



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### 更新日志

1. 	v0.0.x 		初始版本